<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Data Journal Siswa</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> SMK N 1 LIWA.
          <small class="pull-right">Teknik Komputer Dan Jaringan</small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-xs-12">
        <center><address>
          <strong>PEMERINTAH PROVINSI LAMPUNG</strong><br>
          <strong>DINAS PENDIDIKAN DAN KEBUDAYAAN</strong><br>
          SEKOLAH MENENGAH KEJURUAN (SMK) NEGERI 1 LIWA<br>
          Jln. KH Ahmad Dahlan No 142, Padang Dalom, Kec. Balik Bukit, Kab. Lampung Barat<br>
          Email: smknegeri1liwa@gmail.com
        </address></center>
        <hr>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Hari/tanggal</th>
            <th>Waktu</th>
            <th>Kegiatan</th>
          </tr>
          </thead>
          <tbody>
          <?php
            $no = 1; 
            foreach($data as $hasil){ 
          ?>
            <tr>
              <td><?php echo $no++ ?></td>
              <td><?php echo $hasil->hari_tanggal ?></td>
              <td><?php echo $hasil->waktu ?></td>
              <td><?php echo $hasil->kegiatan ?></td>
            </tr>

          <?php } ?>
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Pembimbing :
          <address>
            <br>
            <br>
            <br>
            <br>
            Feri Yana, S.kom<br>
            Nip.
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Kepala Sekolah : 
          <address>
            <br>
            <br>
            <br>
            <br>
            Muhammad Yusuf Muiz, Spd.MM<br>
            Nip. 
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Kepala Jurusan :
          <address>
            <br>
            <br>
            <br>
            <br>
            Didik Kurniawan, S.kom. M.TI<br>
            Nip.
          </address>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
