<div class="content-wrapper">
  <section class="content-header animated fadeIn">
    <h1>
      My To Do List    <small>Manage To Do List</small>
    </h1>
    <?php echo $this->session->flashdata('notif') ?>
    <ol class="breadcrumb">
      <li><a href="<?= base_url(); ?>dashboard/">
      <i class="fa fa-home"></i> Home</a></li>  
    </ol>
  </section>

<section class="content animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Masukkan To do listmu</h3>
          <div class="box-tools pull-right" style="margin-top:2px;">
            <a href="<?php echo base_url() ?>to_do_list/tambah/" class="btn btn-xs bg-blue">Tambah</a>
          </div>
          </h3>
        </div>

        <?php echo form_open('to_do_list/simpan') ?>
        <form role="form">
          <div class="box-body">
            <div class="form-group">
              <label for="text">Target</label>
              <input type="text" name="target" class="form-control" placeholder="Masukkan target">
            </div>
            <div class="form-group">
              <label for="text">Waktu</label>
              <input type="text" name="waktu" class="form-control" placeholder="Masukkan Waktu">
            </div>
          </div>

          <div class="box-footer">
            <button type="submit" class="btn btn-md btn-success">Submit</button>
            <button type="reset" class="btn btn-md btn-warning">reset</button>
            <a href="<?= base_url(); ?>to_do_list" class="btn btn-md btn-primary">Batal</a>
             <?php echo form_close() ?>
          </div>

        </form>
      </div>
    </div>
  </div>
</section>
</div>