<div class="content-wrapper">
  <section class="content-header animated fadeIn">
    <h1>
      My To Do List    <small>Manage To Do List</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="<?= base_url(); ?>dashboard/">
      <i class="fa fa-home"></i> Home</a></li>  
    </ol>
  </section>

<section class="content animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Update To Do Listmu</h3>
        </div>

        <?php echo form_open('to_do_list/update') ?>
			  
		    <input type="hidden" value="<?php echo $to_do_list->id ?>" name="id">
		    <div class="box-body">
		    	<div class="form-group">
            <label for="text">Target</label>
            <input type="text" name="target" value="<?php echo $to_do_list->target ?>" class="form-control" placeholder="Masukkan target">
          </div>
          <div class="form-group">
            <label for="text">Waktu</label>
            <input type="text" name="waktu" value="<?php echo $to_do_list->waktu ?>" class="form-control" placeholder="Masukkan Waktu">
          </div>

          <div class="box-footer">
             <button type="submit" class="btn btn-md btn-success">Update</button>
             <button type="reset" class="btn btn-md btn-warning">Reset</button>
             <a href="<?= base_url(); ?>to_do_list" class="btn btn-md btn-primary">Batal</a>
        	<?php echo form_close() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>