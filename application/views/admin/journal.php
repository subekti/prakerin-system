<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<section class="content-header animated fadeIn">
  <h1>
    Journal    <small>Manage Journal</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= base_url(); ?>journal/">
    <i class="fa fa-home"></i> Home</a></li>
  </ol>
</section>

<section class="content animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">List Journal</h3>
          <div class="box-tools pull-right" style="margin-top:2px;">
            <a href="<?= base_url(); ?>print_data" target="_blank" class="btn btn-xs bg-blue">
              <i class="fa fa-print"></i> Print
            </a>
            <button type="button" class="btn btn-xs bg-blue" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#form-tambah-journal" onclick="submit('tambah')">
              <i class="fa fa-plus-circle"></i> Add
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
          <table id='employeeList' class="table table-bordered table-list">
            <thead class="bg-red">
            <tr>
              <th width="200px">Hari/Tanggal</th>
              <th>Waktu</th>
              <th>Kegiatan</th>
              <th width="150px">Action</th>
            </tr>
            </thead>
            <tbody></tbody>
          </table>
          <br>
          <div id='pagination'></div> 
          <button type="button" class="btn btn-block btn-success btn-sm">Total Data :
            <?php echo $total_asset; ?>
          </button>

        </div>
      </div>
    </div>
  </div>
</section>
</div>

<!-- =========================================================================================================== -->
<!-- ADD MODAL PELANGGAN -->
<div class="modal fade" id="form-tambah-journal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"">
  <div class="modal-dialog" role="document">
    <form method="post" id="form">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="judul-form"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <small id="pesan" class="form-text text-danger text-center"></small>
        <div class="modal-body">
          <div class="form-group">
            <label for="no_rek">Hari/Tanggal</label>
            <input type="text" id="hari_tanggal" name="hari_tanggal" class="form-control" placeholder="Hari/Tanggal">
          </div>
          <div class="form-group">
            <label for="kode_bank">Waktu</label>
            <input type="text" id="waktu" name="waktu" class="form-control" placeholder="Waktu">
          </div>

          <div class="form-group">
            <label for="nama">Kegiatan</label>
            <input type="nama" id="kegiatan" name="kegiatan" class="form-control" placeholder="Kegiatan">
          </div> 
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="button" id="btn-tambah" onclick="tambahdata()" class="btn btn-primary">Tambah</button>
          <button type="button" id="btn-ubah" onclick="ubahData()" class="btn btn-primary">Ubah</button>
          <button type="reset" class="btn btn-warning">Reset</button>          
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- END MODAL -->

<!-- ======================================== FOOTER =====================================================-->

<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.4.0
  </div>
  <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
  reserved.
</footer>

<script src="<?= base_url(); ?>assets/dist/sweetalert2.all.min.js"></script>
<!-- jQuery 3 -->
<script src="<?= base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url(); ?>assets/dist/js/demo.js"></script>
<!-- AdminLTE sweetalert -->
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/1.3.3/sweetalert2.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/0.4.5/sweetalert2.css">
<script type="text/javascript" src="https://cdn.jsdelivr.net/sweetalert2/1.3.3/sweetalert2.min.js"></script>

<!-- ========================================== SCRIPT AJAX ==================================================== -->
<script>
  $(document).ready(() => {
    paginationData()

  createPagination(0);

  $('#pagination').on('click','a',function(e){
    e.preventDefault(); 
    var pageNum = $(this).attr('data-ci-pagination-page');
    createPagination(pageNum);
  });

  function createPagination(pageNum){
    $.ajax({
      url: '<?=base_url()?>index.php/Journal/loadData/'+pageNum,
      type: 'get',
      dataType: 'json',
      success: function(responseData){
        $('#pagination').html(responseData.pagination);
        paginationData(responseData.empData);
      }
    });
  }

  function paginationData(data) {
    $('#employeeList tbody').empty();
    for(emp in data){
      var empRow = "<tr>";
      empRow += "<td>"+ data[emp].hari_tanggal +"</td>";
      empRow += "<td>"+ data[emp].waktu +"</td>";
      empRow += "<td>"+ data[emp].kegiatan +"</td>";
      empRow += "<td>"+ 
            '<button class="btn btn-sm btn-success" onclick="submit('+ data[emp].id +')" data-toggle="modal" data-target="#form-tambah-journal" data-whatever="@getbootstrap"><i class="glyphicon glyphicon-pencil"></i> Edit</button>' +
            '<button class="btn btn-sm btn-danger" onclick="hapus('+ data[emp].id +')"><i class="glyphicon glyphicon-trash"></i> Delete</button>'+ "</td>"; 
      empRow += "</tr>";
      $('#employeeList tbody').append(empRow);          
    }
  }

});

  function tambahdata(){
    let hari_tanggal = $("[name= 'hari_tanggal']").val()
    let waktu = $("[name= 'waktu']").val()
    let kegiatan = $("[name= 'kegiatan']").val()        
      $.ajax({
        type: 'post',
        data: 'hari_tanggal='+ hari_tanggal +'&waktu='+ waktu +'&kegiatan='+ kegiatan,
        dataType: 'JSON',
        url: '<?= base_url(); ?>journal/tambahdata',
        success: function(hasil) {
          if(hasil.msg == ''){
            $('#form-tambah-journal').modal('hide')

              $("[name='hari_tanggal']").val('')
              $("[name='waktu']").val('')
              $("[name='kegiatan']").val('')

              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Your work has been saved',
                showConfirmButton: false,
                timer: 1500
              })
              }else{
                $('#pesan').html(hasil.msg)
              }
        }
      });
    }

  function submit(x){
    if(x == 'tambah'){
      $('#btn-tambah').show()
      $('#btn-ubah').hide()
    }else{
      $('#btn-tambah').hide()
      $('#btn-ubah').show()
        $.ajax({
          type : 'POST',
          data : 'id='+x,
          url  : '<?= base_url(); ?>journal/ambilId',
          dataType : 'JSON',
          success: function(hasil){
            $('#form').attr('data-id', ""+ hasil[0].id+ '')
            $('[name="hari_tanggal"]').val(hasil[0].hari_tanggal)
            $('[name="waktu"]').val(hasil[0].waktu)
            $('[name="kegiatan"]').val(hasil[0].kegiatan)
            $('#pesan').hide()
          }
        })
      }
    }

  function ubahData(){
    let id = $('#form').attr('data-id')
    let hari_tanggal = $("[name= 'hari_tanggal']").val()
    let waktu = $("[name= 'waktu']").val()
    let kegiatan = $("[name= 'kegiatan']").val()
      $.ajax({
        type: 'POST',
        data: {
          'id': id,
          'hari_tanggal': hari_tanggal,
          'waktu': waktu,
          'kegiatan' : kegiatan
        },
        url : '<?= base_url(); ?>journal/ubahData',
        dataType: 'json',
        success: function(hasil){
          console.log(hasil)
          $('#pesan').html(hasil.msg)
          if(hasil.msg = 'jadi'){
            $('#form-tambah-journal').modal('hide')

              Swal.fire({
              position: 'center',
              type: 'success',
              title: 'Your work has been updated',
              showConfirmButton: false,
              timer: 1500
              })
              paginationData();
            }
          }
        })
      }

  function hapus(id) {
    Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
        type: 'POST',
        data: 'id='+id,
        url: '<?= base_url()."journal/hapus" ?>',
        success: function(){
        ambilData()
        }
        });
        Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
        )
        }
      })
    }
</script>