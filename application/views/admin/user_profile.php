<div class="content-wrapper">
  <section class="content-header animated fadeIn">
    <h1>
      My Profile    <small>Manage Profile</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url(); ?>dashboard/">
      <i class="fa fa-home"></i> Home</a></li>
    </ol>
  </section>

<section class="content animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      	<div class="box box-success">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?= base_url(); ?>assets/dist/img/avatar5.png" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $this->session->userdata('user_name'); ?></h3>

              <p class="text-muted text-center">Member Prakerin</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Nama Lengkap</b> <a class="pull-right"><?php echo $this->session->userdata('user_name'); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Nama Email</b> <a class="pull-right"><?php echo $this->session->userdata('user_email');  ?></a>
                </li>
                <li class="list-group-item">
                  <b>Jurusan</b> <a class="pull-right"><?php echo $this->session->userdata('user_jurusan');  ?></a>
                </li>
                <li class="list-group-item">
                  <b>No Telepon</b> <a class="pull-right"><?php echo $this->session->userdata('user_mobile');  ?></a>
                </li>
              </ul>

              <a href="#" class="btn btn-danger btn-block"><b>Sedang Aktif</b></a>
            </div>
          </div>

    </div>
  </div>
</section>
</div>