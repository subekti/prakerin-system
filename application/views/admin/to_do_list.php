<div class="content-wrapper">
  <section class="content-header animated fadeIn">
    <h1>
      To Do List    <small>Manage To Do List</small>
    </h1>
    
    <ol class="breadcrumb">
      <li><a href="<?= base_url(); ?>dashboard/">
      <i class="fa fa-home"></i> Home</a></li>  
    </ol>
  </section>

<section class="content animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Masukkan To do listmu</h3>
          <div class="box-tools pull-right" style="margin-top:2px;">
            <a href="<?php echo base_url() ?>to_do_list/tambah/" class="btn btn-xs bg-blue">Tambah</a>
          </div>
          </h3>
          <div class="box-tools pull-right" style="margin-top:2px;">
          </div>
        </div>

        <div class="table-responsive">
          <table id="table" class="table table-striped table-bordered table-hover">
              <thead class="bg-red">
                <tr>
                  <th>No.</th>
                  <th>Target Harian</th>
                  <th>Waktu Target</th>
                  <th width="140px">Action</th>
                </tr>
              </thead>
              <tbody>

              <?php
                $no = 1; 
                foreach($to_do_list as $hasil){ 
              ?>

              <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $hasil->target ?></td>
                <td><?php echo $hasil->waktu ?></td>
                <td>
                  <a href="<?php echo base_url() ?>to_do_list/edit/<?php echo $hasil->id ?>" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a href="<?php echo base_url() ?>to_do_list/hapus/<?php echo $hasil->id ?>" onclick="javascript: return confirm('Anda yakin hapus ?')" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i> Hapus</a>
                </td>
              </tr>

              <?php } ?>

              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
</div>

