<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_model extends CI_Model {

  	public function get_all()
    {
        $query = $this->db->select("*")
           			  ->from('user')
           			  ->order_by('user_id', 'DESC')
           			  ->get();
        return $query->result();
    }
}