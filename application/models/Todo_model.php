<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todo_model extends CI_model{

    public function get_all()
    {
        $query = $this->db->select("*")
                 ->from('todolist')
                 ->order_by('id', 'DESC')
                 ->get();
        return $query->result();
    }

    public function simpan($data)
    {
        $query = $this->db->insert("todolist", $data);

        if($query){
            return true;
        }else{
            return false;
        }

    }

    public function edit($id)
    {
        $query = $this->db->where("id", $id)
                ->get("todolist");

        if($query){
            return $query->row();
        }else{
            return false;
        }

    }

    public function update($data, $id)
    {
        $query = $this->db->update("todolist", $data, $id);

        if($query){
            return true;
        }else{
            return false;
        }

    }

    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('todolist');
    }

}
