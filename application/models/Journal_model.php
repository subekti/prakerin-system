<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Journal_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function ambildata($table){
        return $this->db->get($table)->result();
    }

    public function getRecord($rowno,$rowperpage) {         
        $this->db->select('*');
        $this->db->from('journal');
        $this->db->limit($rowperpage, $rowno);  
        $query = $this->db->get();          
        return $query->result_array();
    }

    public function getRecordCount() {
        $this->db->select('count(*) as allcount');
        $this->db->from('journal');
        $query = $this->db->get();
        $result = $query->result_array();      
        return $result[0]['allcount'];
    }

    public function hitungJumlahAsset()
    {   
        $query = $this->db->get('journal');
        if($query->num_rows()>0)
        {
          return $query->num_rows();
        }
        else
        {
          return 0;
        }
    }

    public function tambahdata($data, $table)
    {
        return $this->db->insert($table, $data);
    }

    public function ambilId($table, $where){
        $this->db->select('*')->from($table)->where($where);
        return $this->db->get()->result();
    }

    public function updatedata($data, $where, $table ){
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function hapus($where, $table){
        $this->db->where($where);
        $this->db->delete($table);
    }
}