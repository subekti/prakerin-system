<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class To_do_list extends CI_Controller {

    public function __construct(){

        parent ::__construct();
        $this->load->model('Todo_model'); 
    }

    public function index()
    {
        $data = array(

            'title'         => 'Data to_do_list',
            'to_do_list'    => $this->Todo_model->get_all(),

        );

        $this->load->view('admin/template_admin/header', $data);
        $this->load->view('admin/to_do_list');
        $this->load->view('admin/template_admin/footer');
    }

    public function tambah()
    {
        $data = array(

            'title'     => 'Tambah To Do List'
        );

        $this->load->view('admin/template_admin/header');
        $this->load->view('admin/tambah_todo', $data);
        $this->load->view('admin/template_admin/footer');
    }

    public function simpan()
    {
        $data = array(

            'target'            => $this->input->post("target"),
            'waktu'         => $this->input->post("waktu"),

        );

        $this->Todo_model->simpan($data);

        //redirect
        $this->load->view('admin/template_admin/header');
        redirect ('to_do_list');
        $this->load->view('admin/template_admin/footer');

    }

    public function edit($id)
    {
        $id_buku = $this->uri->segment(3);

        $data = array(

            'title'     => 'Edit Data Buku',
            'to_do_list' => $this->Todo_model->edit($id)
        );

        $this->load->view('admin/template_admin/header');
        $this->load->view('admin/edit_todo', $data);
        $this->load->view('admin/template_admin/footer');
    }

    public function update()
    {
        $id['id'] = $this->input->post("id");
        $data = array(

            'target'           => $this->input->post("target"),
            'waktu'         => $this->input->post("waktu"),

        );

        $this->Todo_model->update($data, $id);

        //redirect
        $this->load->view('admin/template_admin/header');
        redirect ('to_do_list');
        $this->load->view('admin/template_admin/footer');

    }

    public function hapus($id)
    {
        $this->Todo_model->delete($id);

        //redirect
        $this->load->view('admin/template_admin/header');
        redirect ('to_do_list');
        $this->load->view('admin/template_admin/footer');
    }

}