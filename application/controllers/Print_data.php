<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Print_data extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Print_model');
	}
	
	public function index()
	{
		$data['title']='Data Prakerin';
		$data['data']=$this->Print_model->selectAll();
		$this->load->view('invoice-print',$data);
	}
}