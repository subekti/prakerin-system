<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_profile extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->helper('url');
		$this->load->model('Profile_model');
	}

	public function index() 
	{
		$data = $this->Profile_model->get_all();

        $this->load->view('admin/template_admin/header', $data);
        $this->load->view('admin/user_profile.php');
        $this->load->view('admin/template_admin/footer');

    }
}