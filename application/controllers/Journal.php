<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet as Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Xlsx;

class Journal extends CI_Controller {
    
    public function __construct()
    {       
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('Journal_model');
    }

    public function ambil()
    {
        echo json_encode($this->Journal_model->ambildata('journal'));
    }

    public function index()
    {
        $data['total_asset'] = $this->Journal_model->hitungJumlahAsset();
        $this->load->view('admin/template_admin/header', $data);
        $this->load->view('admin/journal');
    }

    public function loadData($record=0) {
        $recordPerPage =8;
        if($record != 0){
            $record = ($record-1) * $recordPerPage;
        }       
        $recordCount = $this->Journal_model->getRecordCount();
        $empRecord = $this->Journal_model->getRecord($record,$recordPerPage);
        $config['base_url'] = base_url().'index.php/Journal/loadData';
        $config['use_page_numbers'] = TRUE;

        $config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative;top:-25px;'>";
        $config['full_tag_close']= "</ul>";
        $config['num_tag_open']  = "<li>";
        $config['num_tag_close'] = "</li>";
        $config['cur_tag_open']  = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        
        $config['total_rows'] = $recordCount;
        $config['per_page'] = $recordPerPage;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['empData'] = $empRecord;
        echo json_encode($data);        
    }

    public function tambahdata()
    {
        $hari_tanggal = $this->input->post('hari_tanggal');
        $waktu = $this->input->post('waktu');
        $kegiatan = $this->input->post('kegiatan');
        $result['msg'] = '';
        $data = [
            'hari_tanggal' => $hari_tanggal,
            'waktu'   => $waktu,
            'kegiatan' => $kegiatan
            ];
        if ($hari_tanggal == '') {
            $result['msg'] = 'Hari Tanggal harus diisi';
        } elseif ($waktu == '') {
            $result['msg'] =  'Waktu harus diisi';
        } elseif($kegiatan == '') {
            $result['msg'] = 'Kegiatan harus diisi';
        } else{
            $this->Journal_model->tambahdata($data, 'journal');
        }
            echo json_encode($result);
    }

    public function ambilId()
    {
        $where = ['id' => $this->input->post('id')];
        $dataJournal = $this->Journal_model->ambilId('journal', $where);
        echo json_encode($dataJournal);
    }

    public function ubahData(){
        $id = $this->input->post('id');
        $hari_tanggal = $this->input->post('hari_tanggal');
        $waktu = $this->input->post('waktu');
        $kegiatan = $this->input->post('kegiatan');
        $result['msg'] = '';
        $where = ['id'=>$id];
        $data = [
            'id' => $id,
            'hari_tanggal' => $hari_tanggal,
            'waktu'   => $waktu,
            'kegiatan' => $kegiatan
            ];
            if ($hari_tanggal == '') {
                $result['msg'] = 'Hari Tanggal harus diisi!';
            } elseif ($waktu == '') {
                $result['msg'] =  'Waktu harus diisi';
            } elseif($kegiatan == '') {
                $result['msg'] = 'Kegiatan harus diisi';
            } else{
                $this->Journal_model->updatedata($data, $where, 'journal');
            }
            echo json_encode($data);
    }

    public function hapus(){
        $id = $this->input->post('id');
        $where = ['id' => $id];
        $this->Journal_model->hapus($where, 'journal');
    }
}